#################################################################################
## Force python3
#################################################################################
import sys
if sys.version_info < (3,6):
  raise BaseException("Wrong Python version detected. Please ensure that you are using Python >=3.6!")
#################################################################################

import os
import yaml
import pidfile
import daemon
import sdnotify
import argparse
import signal
import logging
import time
import subprocess

pjoin = os.path.join

PIDFILE_PATH = '/var/run/netconfig_daemon.pid'             # PID File Path (Configured in service file)
LOGFILE_PATH = '/var/log/netconfig_daemon.log'             # Log File Path
CONFIG_FILE_PATH = '/etc/netconfig_daemon'                 # YAML configuration file for this daemon

IFCONFIG = '/usr/sbin/ifconfig'

running = True

# Set up logging for this daemon
LOGGER = logging.getLogger()

formatter = logging.Formatter('%(asctime)s %(message)s', datefmt='%d-%b-%y %H:%M:%S')
file_handler = logging.FileHandler(LOGFILE_PATH, 'a')
file_handler.setFormatter(formatter)

LOGGER.addHandler(file_handler)
LOGGER.setLevel(logging.INFO)

# 
# Default configuration holding a mapping of portName -> portConfiguration.
# This configuration holds the default values for ETH0 and ETH1 ports.
#
# If a config field is not found for a port in the YAML config file,
# the config specified here will be used.
# 
DEFAULT_CONFIG = {}

DEFAULT_CONFIG["connections_file"] = "/opt/address_table/connections.xml"

# Setup default configuration for port eth0
DEFAULT_CONFIG["eth0"] = {
    "default_mac_addr" : "00:50:51:FF:00:00", # Default MAC address to fall back to if an address cannot be read
    "interface_setup"  : {
        "commands" : [
            "ethtool -s eth0 speed 100 duplex full autoneg on"
        ],
    }, # Interface setup commands
    "register_names" : {
        "top" : "SLAVE_I2C.S1.SM",
        "mac_addr" : "BYTE_",
        "checksum" : "CHECKSUM",
        "i2c_done" : "STATUS.I2C_DONE",
    }, # Register name patterns to read the MAC addr and the checksum value
    "eth_mac_dir" : "/fw/SM", # Directory where the "eth*_mac.dat" file(s) are located
    "network_up_wait_time" : 120, # Number of seconds to wait for the network to be up
}

# Setup default configuration for port eth1
DEFAULT_CONFIG["eth1"] = DEFAULT_CONFIG["eth0"].copy()
DEFAULT_CONFIG["eth1"].update({
    "default_mac_addr" : "00:50:51:FF:10:00",
    "interface_setup"  : {
        "commands" : [
            "ethtool -s eth1 speed 1000 duplex full autoneg on"
        ],
    },
})

# 
# The default port to set up. If the configuration is not found under
# /etc/netconfig_daemon, the default configuration under "eth0" port 
# will be used.
# 
DEFAULT_PORT = "eth0"


def run_command(command, verbose=True):
    """
    Run a command (given by a single string value) via subprocess library.
    Returns the stdout of the subprocess call.
    Raises a RuntimeError if the return code of the call is non-zero.
    """
    if verbose:
        LOGGER.info(f'   >>> {command}')
    
    proc = subprocess.Popen(command.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.wait()
    stdout, stderr = proc.communicate()
    
    if proc.returncode != 0:
        LOGGER.error(stderr)
        raise RuntimeError(f'Command failed: {command}')
    
    if verbose:
        LOGGER.info('    -> OK')

    return stdout

def load_config():
    """
    Load the configuration from the YAML configuration file.
    If a field is missing in the config file, it will be taken from the DEFAULT_CONFIG
    dictionary defined in this script.

    If the configuration file is not found under CONFIG_FILE_PATH,
    returns the default configuration for the default port, as defined
    above.

    Returns the configuration as a dictionary object.
    """
    LOGGER.info(f'Loading config from: {CONFIG_FILE_PATH}')
    
    # Config file not found: Return the default configuration for the default port
    if not os.path.exists(CONFIG_FILE_PATH):
        LOGGER.warning(f"Could not find the configuration file under {CONFIG_FILE_PATH}")
        LOGGER.warning(f"Will use the default configuration with port {DEFAULT_PORT}")
        return DEFAULT_CONFIG[DEFAULT_PORT]

    # Load the data from configuration file
    with open(CONFIG_FILE_PATH, "r") as f:
        yaml_config = yaml.safe_load(f)
    
    # Build the configuration
    config = {}

    try:
        config["connections_file"] = yaml_config["connections_file"]
    except KeyError:
        LOGGER.warning(f"connections_file entry not found in config {CONFIG_FILE_PATH}")
        LOGGER.warning(f"Using default connections file: {DEFAULT_CONFIG['connections_file']}")
        config["connections_file"] = DEFAULT_CONFIG["connections_file"]

    # 
    # Loop over each port in the YAML configuration file and retrieve
    # the configurations. If a field is missing in the YAML configuration file,
    # use the one from the DEFAULT_CONFIG object.
    # 
    ports = ['eth0', 'eth1']
    for port in ports:
        if port not in yaml_config:
            continue
        
        port_config = yaml_config[port]
        # Unsupported port name provided in YAML config, skip
        if port not in DEFAULT_CONFIG:
            supported_ports = ", ".join(list(DEFAULT_CONFIG.keys()))
            LOGGER.warning(f"Unrecognized port name: {port}.")
            LOGGER.warning(f"Only supporting ports {supported_ports}.")
            continue
        
        required_fields = DEFAULT_CONFIG[port].keys()
        config[port] = {}
        
        # Loop over fields and build the configuration for this port
        # This loop makes sure that we get all the required fields
        for field in required_fields:
            if field in port_config:
                config[port][field] = port_config[field]
                continue
            
            # Field not found in user config, warn the user and use the default
            LOGGER.warning(f"{CONFIG_FILE_PATH} {port}: Field missing: {field}, using defaults")
            config[port][field] = DEFAULT_CONFIG[port][field]

        # Warn the user if we see unrecognized field names in the 
        # YAML file that we are not using
        user_fields = set(port_config.keys())
        unrecognized_fields = user_fields.difference(required_fields)

        for field in unrecognized_fields:
            LOGGER.warning(f"{CONFIG_FILE_PATH} {port}: Unrecognized config field: {field}")

    return config


def check_if_i2c_writes_are_done(config, port, apollosm=None):
    """
    This function checks if the I2C writes are complete (to be able to 
    read the MAC addr) by reading the "SLAVE_I2C.SM.S1.ETH1_MAC.DONE"
    (or ETH0) register once.
    
    If we get a positive response, this function will return True, 
    otherwise it will return False.
    """
    # We do not have an ApolloSM instance to do the read for this check.
    # Straight out return False so that the daemon could move to the next backup reads
    # for the MAC address.
    if not apollosm:
        return False

    # Obtain the register name to read from the configuration
    # (for the I2C_DONE register)
    port_config = config[port]
    top_register_name = port_config["register_names"]["top"]
    done_register_name = port_config["register_names"]["i2c_done"]

    register_to_read = f"{top_register_name}.{done_register_name}"

    LOGGER.info(f"Checking if I2C writes are done for port: {port}")
    value = apollosm.ReadRegister(register_to_read)

    # If this value is 0x1, the I2C writes are complete
    if value == 1:
        LOGGER.info(f"{register_to_read} 0x1, I2C writes are OK")
        return True

    # If we fall here, we weren't able to read a 0x1 from the
    # I2C_DONE register, return False.
    LOGGER.warning(f'{register_to_read} 0x0, will skip I2C reads')
    return False


class MACAddressValidator():
    def __init__(self, config, apollosm=None) -> None:
        """
        Object to validate a given MAC address.
        
        This is done by comparing the two values:
            - The 8-bit checksum computed from the MAC address (a)
            - The 8-bit checksum read from the ZYNQ's I2C registers (b)
        
        The MAC address is valid if and only if (a+b) & 0xFF == 0
        """
        self.config = config
        self.apollosm = apollosm


    def _read_checksum_from_i2c_registers(self, port):
        """Read 8-bit checksum from the corresponding I2C register."""
        if not self.apollosm:
            raise RuntimeError("ApolloSM instance not found.")

        config_for_port = self.config[port]
        
        # Retrieve the I2C register names to read from the configuration given
        register_names = config_for_port['register_names']
        register_to_read = f"{register_names['top']}.{port.upper()}_MAC.{register_names['checksum']}"

        # Read the 8-bit checksum value and return it as an integer
        checksum = self.apollosm.ReadRegister(register_to_read)

        return checksum

    
    def _compute_checksum_from_mac_addr(self, mac_addr):
        """Compute and return the 8-bit checksum from the given MAC address."""
        # Compute the 8-bit checksum of the MAC address
        tokens = mac_addr.split(':')
        checksum = 0
        
        # The format of these tokens are validated before, so we should be good
        for token in tokens:
            checksum += int(token, base=16)
        
        # Convert the result to 8-bits
        checksum = checksum & 0xFF

        return checksum

    def validate_format(self, mac_addr):
        """Validate the format of the MAC address."""
        tokens = mac_addr.split(':')

        # The MAC address must be a sequence of six 8-bit hex values
        if len(tokens) != 6:
            return False

        for token in tokens:
            if len(token) != 2:
                return False
            
            # Can we convert the hex into a valid int?
            try:
                _ = int(token, base=16)
            except ValueError:
                return False

        return True


    def validate(self, port, mac_addr):
        """
        Validate a MAC address via two checks:
        1. Check the format of the MAC address (i.e. 6 x 8-bit tokens)
        2. Check the checksum as read from the I2C registers.
        -> Returns True/False indicating whether the MAC address is valid.

        If we CANNOT retrieve the checksum for this port, returns FALSE.
        """
        # Format check first
        if not self.validate_format(mac_addr):
            return False

        # If all good, compare the checksums and proceed with validation
        checksum_from_mac = self._compute_checksum_from_mac_addr(mac_addr)
        LOGGER.debug(f'Checksum computed for {port} MAC: 0x{checksum_from_mac:02X}')

        # Obtain the two's complement of the checksum for this port using a
        # BUTool read from the Zynq's I2C registers
        try:
            checksum_from_zynq = self._read_checksum_from_i2c_registers(port)
            LOGGER.debug(f'Checksum read for {port} MAC: 0x{checksum_from_zynq:02X}')
        
        # Handle checksum read failure
        except RuntimeError:
            LOGGER.debug(f'Cannot read checksum for port {port}')
            return False

        # Check if the checksums sum to zero (in 8-bits)
        if (checksum_from_zynq + checksum_from_mac) & 0xFF == 0:
            LOGGER.debug(f'   >>> (0x{checksum_from_mac:02X} + 0x{checksum_from_zynq:02X}) & 0xFF == 0, MAC address is valid for {port}')
            return True

        LOGGER.debug(f"Failed to validate the MAC address for {port}")
        return False


class MACAddressRetriever():
    def __init__(self, config, apollosm=None) -> None:
        """
        Object to retrieve the MAC address of the ZYNQ ETH interface.  
        """
        # Member object to validate the MAC addresses
        self.validator = MACAddressValidator(config, apollosm)
        self.config = config

        # ApolloSM instance to use for doing the register reads. If this is None,
        # do not try to read registers.
        self.apollosm = apollosm

    def _get_from_i2c_registers(self, port):
        """
        Function wrapper to get the ETH0 or ETH1 MAC address using BUTool.
        """
        # Check if we have a valid ApolloSM instance
        if not self.apollosm:
            raise RuntimeError("ApolloSM instance not found, will not try to read the registers.")

        # Retrieve the MAC address bytes from the registers via BUTool
        config_for_port = self.config[port]
        register_names = config_for_port['register_names']

        # We're going to read 6 registers for each byte of the MAC address
        registers_to_read = [f"{register_names['top']}.{port.upper()}_MAC.{register_names['mac_addr']}{i}" for i in range(6)]
        address_bytes = []

        for reg in registers_to_read:
            byte = self.apollosm.ReadRegister(reg) 
            address_bytes.append(byte)
        
        mac_addr = ":".join("{:02X}".format(byte) for byte in address_bytes)

        # Check if the retrieved MAC address is valid
        if self.validator.validate(port, mac_addr):
            return mac_addr

        raise RuntimeError(f"MAC address is not valid: {mac_addr}")

    def _get_from_dat_file(self, filepath):
        """
        Retrieve the MAC address from the given .dat file.
        """
        # The .dat file does not exist
        if not os.path.exists(filepath):
            raise FileNotFoundError(f"File does not exist: {filepath}")

        # Read the value from the .dat file
        with open(filepath, "r") as f:
            mac_addr = f.read().strip()
        
        # Check if the format is valid
        if not self.validator.validate_format(mac_addr):
            raise RuntimeError(f'MAC address format is not valid: {mac_addr}')

        return mac_addr

    def _get_from_config(self, port):
        """
        Retrieve the MAC address from the current configuration.
        """
        # Just return the default MAC address specified by the config.
        # Note that self.config is guaranteed to have a "default_mac_addr" key,
        # because it must come from either the YAML config or the default config.
        return self.config[port]["default_mac_addr"]


    def retrieve(self, port, i2c_writes_done=True):
        """
        Retrieve the MAC address for the given port. 
        
        This will try four scenarios to obtain the MAC address, in the following order:
            1) Read it from ZYNQ's I2C registers
            2) Read it from the eth0_mac.dat (or eth1) file, for which the location should be specified
               in the configuration file.
            3) Take the default for this port from the configuration.

        NOTE: If the i2c_writes_done parameter is False, that indicates that the
        MAC address values are not properly written in Zynq's I2C registers. This will
        trigger this function to skip trying to read from I2C registers.
        """
        LOGGER.info(f'Starting MAC address retrieval for port: {port}')

        # 1) Try retrieving the MAC address from ZYNQ I2C registers via BUTool
        # Do this only if we were able to validate that I2C writes are properly done
        if i2c_writes_done:
            LOGGER.info(f"Trying to read {port} MAC address from ZYNQ I2C registers")
            try:
                mac_addr = self._get_from_i2c_registers(port)
                return mac_addr
            
            except RuntimeError as e:
                LOGGER.error(str(e))
                LOGGER.info("Failed to read the MAC address from ZYNQ I2C registers")

        # 2) Try retrieving the MAC address from the .dat file under /fw/SM directory
        eth_mac_file_path = pjoin(self.config[port]["eth_mac_dir"], f"{port}_mac.dat")
        LOGGER.info(f"Trying to read {port} MAC address from {eth_mac_file_path}")

        try:
            mac_addr = self._get_from_dat_file(eth_mac_file_path)
            return mac_addr
        
        except (FileNotFoundError, RuntimeError):
            LOGGER.info(f"Failed to read the MAC address from {eth_mac_file_path}")

        # 3) If the above two options do not work: Get the dummy MAC address from the configuration
        LOGGER.info(f"Reading {port} MAC address from daemon configuration")
        mac_addr = self._get_from_config(port)

        return mac_addr

def check_interface(port):
    """
    Returns the state of the given port, specifying whether it is up or not.
    """
    with open(f"/sys/class/net/{port}/operstate") as stateFile:
        state = stateFile.readline()
    return state

def update_mac_addr(port, mac_addr, ifup_wait_time):
    """
    Using ifconfig commands, update the MAC address of the ZYNQ network interface.
    Once the MAC address is updated, call the DHCP server to retrieve the new IP address.
    """
    # Set of commands to execute to set up the ETH interface for this port
    commands = [
        f"{IFCONFIG} {port} down",
        f"{IFCONFIG} {port} hw ether {mac_addr}",
        f"{IFCONFIG} {port} up",
    ]

    LOGGER.info(f"Updating network interface for port {port}")   
 
    # Launch the ifconfig commands one by one to update MAC address
    for command in commands:
        run_command(command)
    
    # Monitor this interface to make sure it is up
    # We will wait at most until "end_time" 
    end_time = time.time() + ifup_wait_time

    while "up" not in check_interface(port):
        time.sleep(0.25)
        if time.time() > end_time:
            logging.info(f"Port {port} failed to go up")
            break
    
    status = check_interface(port)
    LOGGER.info(f"Port {port} is {status}")   
 

def setup_interface(port, commands):
    """
    Function to execute subprocess calls for network interface setup.
    
    This is board revision specific, and the list of commands that need to be executed
    should be given as a Python list with the "commands" argument.
    """
    LOGGER.info(f"Setting up interface {port}")   
    for command in commands:
        run_command(command)
  

def func_daemon(config, dryrun=False, apollosm=None):
    """
    The main daemon functionality.
    
    If an ApolloSM (!= None) is passed in, this will use the ApolloSM instance to
    do the reads of MAC address values. Otherwise, it will fall to the backup methods
    (e.g. check the eth1_mac.dat file etc.)
    """
    global running

    def signal_handler(signum, frame):  
        """Custom signal handler. Stops running the daemon once the SIGINT signal is recieved."""
        global running
        LOGGER.info(f"Handler got {str(signum)}")
        LOGGER.info(f"Running is {str(running)}")
        running = False

    # Register the SIGINT signal to the custom signal_handler function
    signal.signal(signal.SIGINT, signal_handler)

    # Object to use for MAC address retrieval for each port
    retriever = MACAddressRetriever(config, apollosm=apollosm)
    
    # 
    # MAIN LOOP: Loop over the list of ports we want to set the interface up.
    # For each port, retrieve the MAC address, and set up the interface via ifconfig commands.
    # Wait for the interface to actually come up before moving on.
    #
    ports = ['eth0', 'eth1']
    for port in ports:
        
        if port not in config.keys():
            continue
        
        # For this port, check if the I2C registers have the MAC address 
        # values yet by reading an I2C_DONE register, and checking if the value is 0x1.
        # If we get a 0x0, we will skip the I2C reads during MAC address retrieval.
        i2c_writes_done = check_if_i2c_writes_are_done(config, port, apollosm)

        # Retrieve the MAC address
        mac_addr = retriever.retrieve(port, i2c_writes_done=i2c_writes_done)

        LOGGER.info(f'ETH port               ->  {port.upper()}')
        LOGGER.info(f'Retrieved MAC address  ->  {mac_addr}')

        # Do not actually update the network interface for dry run
        if dryrun:
            continue

        # Maximum waiting time for the interface to come up
        ifup_wait_time = config[port]["network_up_wait_time"]
        try:
            # Run the interface setup commands, as defined in the YAML
            # config file.
            setup_interface(port, config[port]["interface_setup"]["commands"])

            # Update the MAC address of the interface
            # Wait ifup_wait_time number of seconds (at most) 
            # for the network to come up.
            update_mac_addr(port, mac_addr, ifup_wait_time)
        
        except RuntimeError as e:
            LOGGER.error(str(e))
            LOGGER.error("ETH network setup failed, skipping.")
            pass

    # For dry run, exit here
    if dryrun:
        LOGGER.info("Dry run requested, exiting.")
        sys.exit(0)

    # Inform systemd that we've finished our startup sequence
    # This allows systemd to start other services that depend
    # on this one.
    notifier = sdnotify.SystemdNotifier()
    notifier.notify("READY=1")
  
    # Run in the background until we recieve a SIGINT signal
    while running:
        time.sleep(1)
    
    # SIGINT signal received, shut down
    LOGGER.info("Shutdown")
    sys.exit(0)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--nodaemon", help="Don't daemonize this process", action="store_true")
    parser.add_argument("--dry", help="Dry run, do not update the interface. Has no effect if the process is a daemon", action="store_true")
    args = parser.parse_args()

    LOGGER.info('')
    LOGGER.info('Netconfig daemon: Execution starting.')
    LOGGER.info('')

    # Load the daemon configuration from /etc/netconfig_daemon
    config = load_config()

    # 
    # Import ApolloSM python bindings (if they are there)
    # 
    apollosm = None
    try:
        from ApolloSM import ApolloSM
        apollosm = ApolloSM([config["connections_file"]])
    # Failed to locate/import the ApolloSM Python bindings, do not use them.
    except ModuleNotFoundError:
        LOGGER.warning("Failed to import ApolloSM Python bindings, will not try to use an ApolloSM instance.")
    except RuntimeError as err:
        #Failed to create the apolloSM for some reason.
        #keep going so that the network comes up via the other methods
        LOGGER.warning("Failed to create ApolloSM: ",err)
        
    if args.nodaemon:
        LOGGER.info("Not running as a daemon")
        func_daemon(config, dryrun=args.dry, apollosm=apollosm)

    else:
        # If we are running as daemon, make sure that the file handler
        # is passed in to the DaemonContext as a stream to preserve.
        # Otherwise, the stream gets blocked and no logging is done.
        with daemon.DaemonContext(
            working_directory="/tmp/", 
            pidfile=pidfile.PIDFile(PIDFILE_PATH),
            files_preserve=[file_handler.stream.fileno()]
            ) as context:
            func_daemon(config, apollosm=apollosm)


if __name__ == "__main__":
    main()
