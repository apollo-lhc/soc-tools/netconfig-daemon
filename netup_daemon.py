#################################################################################
## Force python3
#################################################################################
import sys
if sys.version_info < (3,6):
  raise BaseException("Wrong Python version detected. Please ensure that you are using Python >=3.6!")
#################################################################################

import os
import yaml
import pidfile
import daemon
import sdnotify
import argparse
import signal
import logging
import time
import subprocess

pjoin = os.path.join

PIDFILE_PATH = '/var/run/netup_daemon.pid'             # PID File Path (Configured in service file)
LOGFILE_PATH = '/var/log/netup_daemon.log'             # Log File Path

running = True

def func_daemon(nodaemon=False):
  """The main daemon functionality."""
  global running
  
  def signal_handler(signum, frame):  
    """Custom signal handler. Stops running the daemon once the SIGINT signal is recieved."""
    global running
    logging.info(f"Handler got {str(signum)}")
    logging.info(f"Running is {str(running)}")
    running = False
    
  # Register the SIGINT signal to the custom signal_handler function
  signal.signal(signal.SIGINT, signal_handler)
    
  # Inform systemd that we arne't up yet
  notifier = sdnotify.SystemdNotifier()
  while running:

    while running:
      #process the route file to see if it has any info after line 1
      #lines after line 1 are routes
      with open("/proc/net/route") as stateFile:
        for count, lin in enumerate(stateFile):
          pass
        if count > 1:
          #we have a round, say we are ready
          time.sleep(1)
          logging.info("Network is up")
          notifier.notify("READY=1")
          break
      time.sleep(0.25)
    
    #we are now in the read state, but need to watch for the route to dissapear
    while running:
      with open("/proc/net/route") as stateFile:
        for count, lin in enumerate(stateFile):
          pass
        if count < 2:
          #all routes are gone, got back to searching
          logging.info("Network is down")
          notifier.notify("RELOADING=1")
          break
      time.sleep(1)                  

  # SIGINT signal received, shut down
  logging.info("Shutdown")
  sys.exit(0)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--nodaemon", help="Don't daemonize this process", action="store_true")
    args = parser.parse_args()

    if args.nodaemon:
        # Set up the logging
        logging.basicConfig(filename=LOGFILE_PATH, filemode='a', level=logging.DEBUG)
        logging.info("Not running as a daemon")
        func_daemon(nodaemon=True)

    else:
        with daemon.DaemonContext(working_directory="/tmp/", pidfile=pidfile.PIDFile(PIDFILE_PATH)) as context:
            # Set up the logging
            # Do this inside the daemon context so that the logging stream is not blocked by the context.
            logging.basicConfig(filename=LOGFILE_PATH, filemode='a', level=logging.DEBUG)
            func_daemon()


if __name__ == "__main__":
    main()
