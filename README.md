# NetConfig Daemon

## Purpose
This is a `systemd` service daemon that reads the MAC address of the ZYNQ ETH ports at boot time, and sets up the ETH network interface, and get the accordingly assigned IP address from the DHCP server.

## Installation

### Prerequisites

These `python` packages needs to be installed:
* [PyYAML](https://pypi.org/project/PyYAML/)
* [python-pidfile](https://pypi.org/project/python-pidfile/)
* [python-daemon](https://pypi.org/project/python-daemon/)

### File + Service Setup
For the service to be run at boot time by `systemd`, this repository must be installed to the `/opt/netconfig-daemon` path in the Apollo blade. The YAML configuration file must be copied to the `/etc/netconfig_daemon` location, and several symlinks need to be configured for `systemd` usage. The following commands can be used:

```bash
URI=https://gitlab.com/apollo-lhc/soc-tools/netconfig-daemon.git
NAME=netconfig-daemon

cd /opt
git clone --branch main ${URI} ${NAME}

# Copy the YAML config to the right place
cp /opt/${NAME}/netconfig_daemon.yaml /etc/netconfig_daemon

# Set up the symlinks
ln -s /opt/${NAME}/netconfig_daemon.service /etc/systemd/system/netconfig_daemon.service
ln -s /opt/${NAME}/netconfig_daemon.service /etc/systemd/system/multi-user.target.wants/netconfig_daemon.service
```

## The Daemon Process

Primary daemon script for the network configuration daemon is `netconfig_daemon.py`. This script utilizes [python-daemon](https://pypi.org/project/python-daemon/) `DaemonContext` instance to be configured properly and run as a Unix daemon process. This daemon can be configured using the fields in `netconfig_daemon.yaml` file.

This script starts by loading the YAML configuration file, which has mapping of port names to port configuration per port. For each port, the daemon does the following:

* Utilizes `BUTool` to read ZYNQ's I2C registers to obtain the MAC address. The register names are listed under `register_names` field in the configuration for each port.
* If this operation fails, the daemon will look at a `${PORT_NAME}_mac.dat` file under `eth_mac_dir`, which has the MAC address for the specified port.
* If the `.dat` file does not exist, the daemon will fall into the `default_mac_addr` for the given port.

### Check Daemon Status

To check that whether the daemon has been running upon boot, the following commands are useful:

```bash
# Get the status of the service
systemctl status netconfig_daemon.service

# Get the logs for this service
journalctl -u netconfig_daemon.service
```



## Daemons ##
This repo has 3 daemons that are used to make sure the network is correctly configured before we run the system.

### netconfig_daemon ###
This daemon runs first and is in charge of setting up the network interfaces and making sure they come up.

### dhclient ###
This daemon comes up after the netconfig_daemon has brought up the network interfaces and starts the dhclient process to run dhcp on each interface

### netup_daemon ###
This daemon comes up after dhclient and does a somewhat hacky job of figuring out if there is a real network connection up and running. 
It does this by looking at the current routing table and seeing if there are any routes in it (similar to the "route" command)