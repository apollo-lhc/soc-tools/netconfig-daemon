#
# Spec file for network configuration daemon RPMs.
#
Name: %{name} 
Version: %{version} 
Release: %{release} 
Packager: %{packager}
Summary: NetconfigDaemon
License: Apache License
Group: Apollo-LHC
Source: https://gitlab.com/apollo-lhc/soc-tools/netconfig-daemon
URL: https://gitlab.com/apollo-lhc/soc-tools/netconfig-daemon
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot 
Prefix: %{_prefix}

%description
Network configuration daemon to set up and validate ethernet interfaces for the Zynq upon boot.

%prep

# Ensure Python3.6 during compiling
%global __python %{__python36}

%build

%install 
# Copy includes to RPM_BUILD_ROOT and set aliases
mkdir -p $RPM_BUILD_ROOT%{_prefix}

# Copy files over
cp -rp %{py_sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.
cp -rp %{systemd_sources_dir}/* $RPM_BUILD_ROOT%{_prefix}/.

%clean 

%post 

%postun 

%files 
%defattr(-, root, root)
%{_prefix}/netconfig_daemon.service
%{_prefix}/netconfig_daemon.py
%{_prefix}/netup_daemon.service
%{_prefix}/netup_daemon.py
%{_prefix}/dhclient.service